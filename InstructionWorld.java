import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.ArrayList;
import java.util.*;
import java.io.*;

/**
 * Write a description of class StartScreen here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class InstructionWorld extends World
{

    /**
     * Constructor for objects of class StartScreen.
     * 
     */
    public InstructionWorld() throws IOException
    {    
        super(1280, 720, 1); 
        prepare();
        setBackground(new GreenfootImage("InstructionsBackground.png"));
    }
    
    private void prepare() throws IOException
    {
        MainMenuNoFade menubutton = new MainMenuNoFade();
        addObject(menubutton,200,100);
        Cheat cheat = new Cheat();
        addObject(cheat, 1120, 30);
    }
}
