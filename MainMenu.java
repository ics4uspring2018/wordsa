import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.ArrayList;
import java.util.*;
import java.io.*;

/**
 * This Actor serves as a main menu return button during the game over screen
 * 
 * @author Chengze Cai, Michael Zou
 * @version April 15 2018
 */
public class MainMenu extends Actor
{
    GreenfootImage mainmenu;
    private boolean ended;
    private int transparency;
    /**
     * Constructor for objects of class MainMenu.
     */
    public MainMenu(){
        mainmenu = new GreenfootImage("Main Menu Button.png");
        setImage(mainmenu);
        transparency = 0;
        getImage().setTransparency(0);
        ended = false;
    }
    
    /**
     * Act - do whatever the MainMenu wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act(){
        transparency += 2;
        if(transparency < 255)
        {
            getImage().setTransparency(transparency);
        }
        else if (ended)
        {
            
            ended = true;
        }
        
        try{
            if (Greenfoot.mouseClicked(this))
            {
                Greenfoot.setWorld(new StartScreen());
            }
        }
        catch(IOException ioe){
            Greenfoot.stop();
        }
      
    }    
}
