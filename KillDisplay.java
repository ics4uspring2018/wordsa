import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
/**
 * Object that displays the number of zombies killed.
 * 
 * @author Chengze Cai
 * @version April 4, 2018
 */
public class KillDisplay extends Actor
{
    /**
     * Act - do whatever the Score wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        GameStats stats = (GameStats) getWorld().getObjects(GameStats.class).get(0);
        String score = Integer.toString(stats.getZombiesKilled());
        Color clear = new Color(0,0,0,0);
        setImage(new GreenfootImage("Zombies Killed: " + score, 20, Color.WHITE, clear));
    }    
}