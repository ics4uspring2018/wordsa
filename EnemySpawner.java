import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.Random;

/**
 * An object that spawns zombies for the user to fight.
 * 
 * @author Michael Zou
 * @version April 4, 2018
 */
public class EnemySpawner extends Actor
{
    //variables that are used to handle normal sapwns
    private int counter;
    private int maxTime;

    //variables that are used to handle waves
    private boolean waving;
    private int waveTimer;
    private boolean firstWave;
    private boolean secondWave;
    private boolean thirdWave;

    /**
     * Constructor for objects of class EnemySpawner, initializes the variables.
     */
    public EnemySpawner(){
        counter = 0;
        maxTime = 100;

        waving = false;
        waveTimer = 0;
    }

    /**
     * Act - do whatever the EnemySpawner wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        GameStats stats = (GameStats) getWorld().getObjects(GameStats.class).get(0);
        Player player = (Player) getWorld().getObjects(Player.class).get(0);
        int numKilled = stats.getZombiesKilled();

        //if its not waving, and the number of zombies killed is within a certain range, and the player has the rifle at least, start waving
        if(!waving && numKilled % 30 > 26 && numKilled % 30 < 29 && player.getWeapon() >= 2){
            waving = true;
        }

        if(!waving){
            //progressively make the game harder as more zombies are killed
            //when you kill 1000 zombies, a zombie spawns every act
            maxTime = 100 - numKilled/10;
            
            counter++;
            if(counter > maxTime){
                //if statements to check if which zombies should be spawned
                if(numKilled > 75){
                    spawnNormalFastOrStrongEnemyLvThree();
                }
                else if(numKilled > 50){
                    spawnNormalFastOrStrongEnemyLvTwo();
                }
                else if(numKilled > 25){
                    spawnNormalFastOrStrongEnemyLvOne();
                }
                else if(numKilled > 10){
                    spawnNormalOrFastEnemy();
                }
                else{
                    spawnNormalEnemy();
                }

                counter = 0;
            }
        }
        else{
            //initialize the big wave, select which wave it will be, as well as show the warning message
            if(waveTimer == 0){
                BigWaveWarning bww = new BigWaveWarning();
                getWorld().addObject(bww, 640, 460);
                Random rand = new Random();
                
                int temp = rand.nextInt(3);
                if(temp == 0)
                    firstWave = true;
                else if(temp == 1)
                    secondWave = true;
                else
                    thirdWave = true;
            }

            waveTimer++;

            //the first wave, spawing 10 normal zombies across the map
            if(waveTimer == 200 && firstWave){
                for(int i = 0; i<10; i++){
                    int[] loc = chooseLocation();
                    Zombie Z = new Zombie();
                    getWorld().addObject(Z, loc[0], loc[1]);
                }
            }

            //the second wave, spawn a sequence of fast zombies 
            if(waveTimer % 40 == 0 && secondWave && waveTimer > 200){
                int[] loc = chooseLocation();
                FastZombie Z = new FastZombie();
                getWorld().addObject(Z, loc[0], loc[1]);
            }

            //the third wave, spawn a 3 strong zombies initially, and then 8 fast ones after a bried delay
            if(waveTimer == 200 && thirdWave){
                for(int i = 0; i<3; i++){
                    int[] loc = chooseLocation();
                    StrongZombie Z = new StrongZombie();
                    getWorld().addObject(Z, loc[0], loc[1]);
                }
            }
            else if(waveTimer == 400 && thirdWave){
                for(int i = 0; i<8; i++){
                    int[] loc = chooseLocation();
                    FastZombie Z = new FastZombie();
                    getWorld().addObject(Z, loc[0], loc[1]);
                }
            }

            //kill the wave and return to normal
            if(waveTimer > 800){
                waveTimer = 0;
                waving = false;
                firstWave = false;
                secondWave = false;
                thirdWave = false;
            }
        }
    }   

    /**
     * Method that can only spawn a normal zombie.
     */
    public void spawnNormalEnemy(){
        int[] loc = chooseLocation();
        Zombie Z = new Zombie();
        getWorld().addObject(Z, loc[0], loc[1]);
    }

    /**
     * Method that can spawn a normal pr fast zombie.
     */
    public void spawnNormalOrFastEnemy(){
        Random rand = new Random();
        int temp = rand.nextInt(4);

        int[] loc = chooseLocation();

        if(temp == 0){
            FastZombie Z = new FastZombie();
            getWorld().addObject(Z, loc[0], loc[1]);
        }
        else{
            Zombie Z = new Zombie();
            getWorld().addObject(Z, loc[0], loc[1]);
        }
    }    

    /**
     * Method that can only spawn a normal, fast or strong zombie. 
     * The ratios are 1:1:3
     */
    public void spawnNormalFastOrStrongEnemyLvOne(){
        Random rand = new Random();
        int temp = rand.nextInt(5);

        int[] loc = chooseLocation();

        if(temp == 0){
            FastZombie Z = new FastZombie();
            getWorld().addObject(Z, loc[0], loc[1]);
        }
        else if(temp == 1){
            StrongZombie Z = new StrongZombie();
            getWorld().addObject(Z, loc[0], loc[1]);
        }
        else{
            Zombie Z = new Zombie();
            getWorld().addObject(Z, loc[0], loc[1]);
        }
    }

    /**
     * Method that can spawn a normal, fast or strong zombie. 
     * The ratios are 1:1:2
     */
    public void spawnNormalFastOrStrongEnemyLvTwo(){
        Random rand = new Random();
        int temp = rand.nextInt(4);

        int[] loc = chooseLocation();

        if(temp == 0){
            FastZombie Z = new FastZombie();
            getWorld().addObject(Z, loc[0], loc[1]);
        }
        else if(temp == 1){
            StrongZombie Z = new StrongZombie();
            getWorld().addObject(Z, loc[0], loc[1]);
        }
        else{
            Zombie Z = new Zombie();
            getWorld().addObject(Z, loc[0], loc[1]);
        }
    }

    /**
     * Method that can spawn a normal, fast or strong zombie. 
     * The ratios are 1:1:1
     */
    public void spawnNormalFastOrStrongEnemyLvThree(){
        Random rand = new Random();
        int temp = rand.nextInt(3);

        int[] loc = chooseLocation();

        if(temp == 0){
            FastZombie Z = new FastZombie();
            getWorld().addObject(Z, loc[0], loc[1]);
        }
        else if(temp == 1){
            StrongZombie Z = new StrongZombie();
            getWorld().addObject(Z, loc[0], loc[1]);
        }
        else{
            Zombie Z = new Zombie();
            getWorld().addObject(Z, loc[0], loc[1]);
        }
    }

    /**
     * Method that randomly chooses a point along the top half of the border.
     * 
     * @return     an array that stores the coordinates
     */
    public int[] chooseLocation(){
        int[] coordinates = new int[2];

        Random rand = new Random();
        int whichBorder = rand.nextInt(3);
        if(whichBorder == 0){
            //Spawns from the left border.
            coordinates[0] = 0;
            coordinates[1] = rand.nextInt(getWorld().getHeight()/2);
        }
        else if(whichBorder == 1){
            //Spawns from the right border.
            coordinates[0] = getWorld().getWidth();
            coordinates[1] = rand.nextInt(getWorld().getHeight()/2);
        }
        else if(whichBorder == 2){
            //Spawns from the top border.
            coordinates[0] = rand.nextInt(getWorld().getWidth());
            coordinates[1] = 0;
        }

        return coordinates;
    }
}
