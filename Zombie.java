import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The "normal" zombie that spawns, average speed and health.
 * 
 * @author Michael Zou
 * @version April 15, 2018
 */
public class Zombie extends Enemy
{    
    /**
     * Constructor for objects of class Zombie, initializes the variables.
     */
    public Zombie(){
        //initialize variables
        attackCooldown = 60;
        animationCount = 1;
        frame  = 1;
        speed = 1;
        damage = 10;
        score = 10;
        health = 100;
        walkAnimateSpeed = 5;
        attackAnimateSpeed = 5;
        
        walk = new GreenfootImage[17];
        for(int i = 1; i<= 17; i++)
            walk[i-1] = new GreenfootImage("ZombieWalk" + i + ".png");
            
        attack = new GreenfootImage[8];
        for(int i = 1; i<= 8; i++)
            attack[i-1] = new GreenfootImage("ZombieAttack" + i + ".png");
    }

    /**
     * Act - do whatever the Zombie wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        turnTowardsPlayer();
        checkTouching();
        if(!touchingPlayer){
            move(speed);
            animateWalk();
        }else{
            animateAttack();
            attack();
        }
        if(health<=0){
            increaseScore();
            GameStats stats = (GameStats) getWorld().getObjects(GameStats.class).get(0);
            stats.setZombieKilled(stats.getZombiesKilled() + 1);
            getWorld().addObject(new Bloodstain(), getX(), getY());
            getWorld().removeObject(this);
        }
    }
}
