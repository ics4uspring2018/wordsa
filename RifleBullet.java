import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The "Rifle" bullet that is created when weapon variable is 1, enough to 1HKO normal zombies and pierce.
 * 
 * @author Michael Zou
 * @version April 5, 2018
 */
public class RifleBullet extends Projectile
{
    /**
     * Constructor for objects of class PistolBullet, initializes the damage.
     */
    public RifleBullet(){
        super();
        damage = 100;
        speed = 40;
        deviation = 1;
        bulletHealth = 120;
    }
    
    /**
     * Act - do whatever the PistolBullet wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        if(!lockedOn){
            turnTowardsEnemy(deviation);
            lockedOn = true;
        }
        move(speed);
        remove();
    }    
}
