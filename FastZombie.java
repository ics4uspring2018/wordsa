import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Class for creating the fast zombie actor.
 * 
 * @author Michael Zou
 * @version April 14, 2018
 */
public class FastZombie extends Enemy
{
    /**
     * Constructor for objects of class FastZombie, initializes the variables.
     */
    public FastZombie(){
        //initialize variables
        attackCooldown = 20;
        animationCount = 1;
        frame  = 1;
        speed = 4;
        damage = 5;
        score = 15;
        health = 50;
        walkAnimateSpeed = 3;
        attackAnimateSpeed = 3;

        walk = new GreenfootImage[17];
        for(int i = 1; i<= 17; i++)
            walk[i-1] = new GreenfootImage("FastZombieWalk" + i + ".png");
            
        attack = new GreenfootImage[8];
        for(int i = 1; i<= 8; i++)
            attack[i-1] = new GreenfootImage("FastZombieAttack" + i + ".png");
    }

    /**
     * Act - do whatever the Zombie wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        turnTowardsPlayer();
        checkTouching();
        if(!touchingPlayer){
            move(speed);
            animateWalk();
        }else{
            animateAttack();
            attack();
        }
        if(health<=0){
            increaseScore();
            GameStats stats = (GameStats) getWorld().getObjects(GameStats.class).get(0);
            stats.setZombieKilled(stats.getZombiesKilled() + 1);
            getWorld().addObject(new Bloodstain(), getX(), getY());
            getWorld().removeObject(this);
        }
    }
}
