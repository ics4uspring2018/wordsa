import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.ArrayList;
import java.util.*;
import java.io.*;

/**
 * The world that the game is played in.
 * 
 * @author Chengze Cai, Brice Zhao, Michael Zou
 * @version April 4, 2016
 */
public class GameWorld extends World
{
    private ArrayList<String> simpleWords = new ArrayList<String>();
    private ArrayList<String> mediumWords = new ArrayList<String>();
    private ArrayList<String> hardWords = new ArrayList<String>();

    /**
     * Constructor for objects of class GameWorld.
     */
    public GameWorld() throws IOException
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(1280, 720, 1); 
        setBackground(new GreenfootImage("backgrounddetailed5.png"));
        prepare();
    }

    /**
     * Prepare the world for the start of the program.
     * That is: create the initial objects and add them to the world.
     */
    private void prepare() throws IOException
    {
        Greenfoot.setSpeed(50);
        
        WordStorage wordstorage = new WordStorage();
        addObject(wordstorage, 0, 700);

        GameRunner rounds = new GameRunner();
        addObject(rounds, 0, 700);

        GameStats stats = new GameStats(100, 0, 0);
        addObject(stats, 0, 700);
        
        KillDisplay kill = new KillDisplay();
        addObject(kill, 1000, 635); 
        
        ScoreDisplay score = new ScoreDisplay();
        addObject(score, 1000, 670);        

        HUD hud = new HUD();
        addObject(hud,640,668);
        
        Player player = new Player();
        addObject(player, 1280/2, 500);
        
        Sandbags sandbag = new Sandbags();
        addObject(sandbag, 1280/2, 500);
        
        EnemySpawner spawner = new EnemySpawner();
        addObject(spawner, 1280/2, 700);
        
        HealthBar healthbar = new HealthBar();
        addObject(healthbar,152,672);
        
        Cheat cheat = new Cheat();
        addObject(cheat, 1120, 30);

        setPaintOrder(GameOver.class, TryAgain.class, MainMenu.class, DimmerBox.class, Enemy.class, Player.class, KillDisplay.class, HealthBar.class, ScoreDisplay.class, Projectile.class, TargetLetter.class, HUD.class, Bloodstain.class, GameStats.class, GameRunner.class, WordManager.class, WordStorage.class);
    }
}
