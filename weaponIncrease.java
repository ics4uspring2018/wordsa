import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Actor is the visual indicator for when the players weapon levels up
 * 
 * @author Chengze Cai
 * @version April 15, 2018
 */
public class WeaponIncrease extends Actor
{
    int x;
    int y;
    int transparency;
    GreenfootImage weaponup;
    
    /**
     * Constructor of objects of class weaponIncrease, initializes the variables.
     */
    public WeaponIncrease(){
        weaponup = new GreenfootImage("weaponup.png");
        setImage(weaponup);
        
        x = 0;
        y = 0;
        transparency = 255;
    }
    
    /**
     * Act - do whatever the weaponIncrease wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        x = getX();
        y = getY();
        setLocation(x,y-1);
        transparency = transparency - 5;
        getImage().setTransparency(transparency);
        if(transparency == 0){
            getWorld().removeObject(this);
            return;
        }
    }    
}
