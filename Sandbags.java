import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Sandbag that appears on the screen.
 * 
 * @author Michael Zou
 * @version April 15, 2018
 */
public class Sandbags extends Actor
{
    GreenfootImage solidSandbags;
    GreenfootImage blackSandbags;

    /**
     * Constructor of Sandbags class, sets the image to be a solid sandbag.
     */
    public Sandbags(){
        solidSandbags = new GreenfootImage("SandbagsSolid.png");
        blackSandbags = new GreenfootImage("SandbagsBlack.png");
        setImage(solidSandbags);
    }

    /**
     * Act - do whatever the Sandbags wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        GameStats stats = (GameStats) getWorld().getObjects(GameStats.class).get(0);
        if(stats.getHealth() <= 0){
            setImage(blackSandbags);
        }
    }    
}
