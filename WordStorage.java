import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.ArrayList;
import java.util.*;
import java.io.*;

/**
 * Object that stores the words in ArrayLists.
 * 
 * @author Michael Zou
 * @version March 24, 2018
 */
public class WordStorage extends Actor
{
    //boolean variable that checks if the word has been typed or not
    private boolean wordDone = true;

    //int variable that stores the number of words solved
    private int numWords = 0;

    //ArrayLists that stores the words
    private static ArrayList<String> simpleWords = new ArrayList<String>();
    private static ArrayList<String> mediumWords = new ArrayList<String>();
    private static ArrayList<String> hardWords = new ArrayList<String>();

    /**
     * Constructor for objects of class WordStorage, reads in all the words from txt files.  
     */
    public WordStorage() throws IOException{
        readSimpleWords();
        readMediumWords();
        readHardWords();
        wordDone = true;
    }

    /**
     * Accessor Method for the wordDone variable.
     */
    public boolean getWordDone(){
        return wordDone;
    }

    /**
     * Mutator Method for the wordDone variable.
     */
    public void setWordDone(boolean bool){
        wordDone = bool;
    }

    /**
     * Method that reads in the simple words from the txt file SimpleWords.txt and 
     * stores it in the ArrayList simpleWords.
     */
    public void readSimpleWords() throws IOException
    {
        Scanner input = getScanner("SimpleWords.txt");
        while(input.hasNextLine()){
            simpleWords.add(input.nextLine());
        }
    }

    /**
     * Method that reads in the simple words from the txt file MediumWords.txt and 
     * stores it in the ArrayList simpleWords.
     */
    public void readMediumWords() throws IOException
    {
        Scanner input = getScanner("MediumWords.txt");
        while(input.hasNextLine()){
            mediumWords.add(input.nextLine());
        }
    }

    /**
     * Method that reads in the simple words from the txt file HardWords.txt and 
     * stores it in the ArrayList simpleWords.
     */
    public void readHardWords() throws IOException
    {
        Scanner input = getScanner("HardWords.txt");
        while(input.hasNextLine()){
            hardWords.add(input.nextLine());
        }
    }

    /**
     * Method that randomly chooses a word from the simpleWords ArrayList
     * 
     * @return     a word from the easyWords ArrayList
     */
    public String getSimpleWord(){
        Random rand = new Random();
        int temp = rand.nextInt(simpleWords.size());
        return simpleWords.get(temp);
    }

    /**
     * Method that randomly chooses a word from the mediumWords ArrayList
     * 
     * @return     a word from the mediumWords ArrayList
     */
    public String getMediumWord(){
        Random rand = new Random();
        int temp = rand.nextInt(mediumWords.size());
        return mediumWords.get(temp);
    }

    /**
     * Method that randomly chooses a word from the hardWords ArrayList
     * 
     * @return     a word from the hardWords ArrayList
     */
    public String getHardWord(){
        Random rand = new Random();
        int temp = rand.nextInt(hardWords.size());
        return hardWords.get(temp);
    }

    /**
     * Opens a text file inside the package folder and returns a scanner to read it. Works for text files inside jar files.
     * 
     * @param name The name of the text file
     * @return A Scanner object that you can use to read the contents of the text file.
     */
    public Scanner getScanner(String filename){
        InputStream myFile = getClass().getResourceAsStream(filename);
        if(myFile != null){
            return new Scanner(myFile);
        }
        return null;
    }
}