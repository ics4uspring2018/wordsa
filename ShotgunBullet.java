import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The "Shotgun" bullet that is created when weapon variable is 1, enough to 2HKO normal zombies but with increased spread.
 * 
 * @author Michael Zou
 * @version April 5, 2018
 */
public class ShotgunBullet extends Projectile
{
    /**
     * Constructor for objects of class PistolBullet, initializes the damage.
     */
    public ShotgunBullet(){
        super();
        damage = 80;
        speed = 25;
        deviation = 20;
        bulletHealth = 1;
    }
    
    /**
     * Act - do whatever the PistolBullet wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        if(!lockedOn){
            turnTowardsEnemy(deviation);
            lockedOn = true;
        }
        move(speed);
        remove();
    }    
}
