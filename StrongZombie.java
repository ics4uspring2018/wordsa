import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Class for Strong Zombie Types
 * 
 * @author Michael Zou
 * @version April 14, 2018
 */
public class StrongZombie extends Enemy
{
    /**
     * Constructor for objects of class Zombie, initializes the variables.
     */
    public StrongZombie(){
        //initialize variables
        attackCooldown = 60;
        animationCount = 0;
        frame  = 1;
        speed = 1;
        damage = 20;
        score = 15;
        health = 200;
        walkAnimateSpeed = 7;
        attackAnimateSpeed = 7;

        walk = new GreenfootImage[17];
        for(int i = 1; i<= 17; i++)
            walk[i-1] = new GreenfootImage("StrongZombieWalk" + i + ".png");
            
        attack = new GreenfootImage[8];
        for(int i = 1; i<= 8; i++)
            attack[i-1] = new GreenfootImage("StrongZombieAttack" + i + ".png");
    }

    /**
     * Act - do whatever the Zombie wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        turnTowardsPlayer();
        checkTouching();
        if(!touchingPlayer){
            move(speed);
            animateWalk();
        }else{
            animateAttack();
            attack();
        }
        if(health<=0){
            increaseScore();
            GameStats stats = (GameStats) getWorld().getObjects(GameStats.class).get(0);
            stats.setZombieKilled(stats.getZombiesKilled() + 1);
            getWorld().addObject(new Bloodstain(), getX(), getY());
            getWorld().removeObject(this);
        }
    }
}
