import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.io.*;
import java.util.ArrayList;

/**
 * Button that destroys all zombies on the screen.
 * 
 * @author Chengze Cai, Brice Zhao, Michael Zou
 * @version April 15, 2018
 */
public class Cheat extends Actor
{
    GreenfootImage cheat;

    /**
     * Constructors for objects of class Cheat, initializes the image.
     */
    public Cheat(){
        cheat = new GreenfootImage("Cheat Button.png");
        setImage(cheat);
    }

    /**
     * Act - do whatever the StartButton wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act(){
        if (Greenfoot.mouseClicked(this))
        {
            ArrayList<Enemy> enemies = (ArrayList<Enemy>) getWorld().getObjects(Enemy.class);
            for(Enemy e: enemies)
                e.takeDamage(1000);
        }
    }    
}
