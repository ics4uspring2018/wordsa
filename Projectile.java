import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.ArrayList;
import java.util.Random;
/**
 * Superclass of all Projectiles
 * 
 * @author Michael Zou
 * @version April 5, 2016
 */
public abstract class Projectile extends Actor
{
    protected boolean lockedOn;
    protected int damage;
    protected int speed;
    protected int deviation;
    protected int bulletHealth;
    
    Random random = new Random();
    /**
     * Constructor for objects of class Projectiles, initializes the lockedOn variable.
     */
    public Projectile(){
        lockedOn = false;
    }

    /**
     * Method that turns the projectile towards the closets enemy.
     */
    public void turnTowardsEnemy(int d){
        int temp = random.nextInt(d*2)-d;
        Player player = (Player) getWorld().getObjects(Player.class).get(0);
        setRotation(player.getRotation()+temp);
        
        GreenfootImage bullet = new GreenfootImage("projectile.png");
        setImage(bullet);
    }

    /**
     * Method that checks if the projectile should be removed from the world. 
     * Remove this object if its out of bounds or comes into contact with an
     * enemy.
     */
    public void remove(){
        Enemy a = (Enemy) getOneIntersectingObject(Enemy.class);
        if(a != null){ 
            if(bulletHealth - a.getHealth() <= 0){
                getWorld().removeObject(this);
                a.takeDamage(damage);
            }else{
                bulletHealth =- a.getHealth();
                a.takeDamage(damage);
            }
            return;
        }

        if(getX() <= 5 || getX() >= getWorld().getWidth()-5 || getY() <= 5 || getY() >= getWorld().getHeight()-5){
            getWorld().removeObject(this);
            return;
        }
    }
}
