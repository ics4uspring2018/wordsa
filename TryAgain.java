import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.ArrayList;
import java.util.*;
import java.io.*;

/**
 * Actor for the try again button on the game over screen.
 * 
 * @author Chengze Cai, Brice Zhao
 * @version (April 15, 2018
 */
public class TryAgain extends Actor
{
    private boolean ended;
    private int transparency;
    GreenfootImage tryagain;
    
    /**
     * Constructor for objects of class TryAgain.
     */
    public TryAgain(){
        transparency = 0;
        getImage().setTransparency(0);
        ended = false;
        
        tryagain = new GreenfootImage("Try Again Button.png");
        setImage(tryagain);
    }
    
    /**
     * Act - do whatever the TryAgain wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act(){
        transparency += 2;
        if(transparency < 255)
        {
            getImage().setTransparency(transparency);
        }
        else if (ended)
        {
            
            ended = true;
        }
        
        try{
            if (Greenfoot.mouseClicked(this))
            {
                Greenfoot.setWorld(new GameWorld());
            }
        }
        catch(IOException ioe){
            Greenfoot.stop();
        }
      
    }    
}
