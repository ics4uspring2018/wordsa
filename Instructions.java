import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.io.*;

/**
 * Button that takes the user to the Instructions World.
 * 
 * @author Chengze Cai, Brice Zhao, Michael Zou
 * @version April 15, 2018
 */
public class Instructions extends Actor
{
    GreenfootImage instructions;
    
    /**
     * Constructors for objects of class Instructions, initializes the image.
     */
    public Instructions(){
        instructions = new GreenfootImage("Instructions Button.png");
        setImage(instructions);
    }
    
    /**
     * Act - do whatever the StartButton wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act(){
        try{
            if (Greenfoot.mouseClicked(this))
            {
                Greenfoot.setWorld(new InstructionWorld());
            }
        }
        catch(IOException ioe){
            Greenfoot.stop();
        }
    }    
}