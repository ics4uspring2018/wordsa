import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Actor that serves to darken the screen once the game is over.
 * 
 * @author Brice Zhao
 * @version April 15, 2018
 */
public class DimmerBox extends Actor
{
    private boolean ended;
    int transparency;
    GreenfootImage box;
    /**
     * Constructor for objects of class DimmerBox, initializes the variables.
     */
    public DimmerBox()
    {
        transparency = 0;
        getImage().setTransparency(0);
        ended = false;
        
        box = new GreenfootImage("Black Box.png");
        setImage(box);
    }

    /**
     * Act - do whatever the DimmerBox wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        transparency++;
        if(transparency < 100)
        {
            getImage().setTransparency(transparency);
        }
        else if(!ended)
        {
            GameOver gameover = new GameOver();
            getWorld().addObject(gameover, 640, 290);
            ended = true;
        }
    }
}
