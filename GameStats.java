import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Class that stores score-related information. Health, number of zombies killed, and score. 
 * 
 * @author Chengze Cai, Michael Zou
 * @version April 4, 2018
 */
public class GameStats extends Actor
{
    private int health;
    private int numZombiesKilled;
    private int score;
    private boolean ended;
    
    private boolean weaponLevelHandgun = true;
    private boolean weaponLevelShotgun = false;
    private boolean weaponLevelRifle = false;

    /**
     * Constructor for GameStats
     * 
     * @param  h      a int value that represents the health of the player
     * @param  z      a int value that represents the number of zombies that the player has killed
     * @param  s      a int value that represents the score
     */
    public GameStats(int h, int z, int s){
        health = h;
        numZombiesKilled = z;
        score = s;
        ended = false;
    }

    /**
     * Accessor method for the 'health' variable.
     * 
     * @return     the health variable
     */
    public int getHealth(){ 
        return health;
    }

    /**
     * Accessor method for the 'numZombiesKilled' variable.
     * 
     * @return     the numZombiesKilled variable
     */
    public int getZombiesKilled(){
        return numZombiesKilled;
    }

    /**
     * Accessor method for the 'score' variable.
     * 
     * @return     the score variable
     */
    public int getScore(){
        return score;
    }

    /**
     * Mutator method for the 'health' variable.
     * 
     * @param  h    the new value of variable health
     */
    public void setHealth(int h){
        health = h;
    }

    /**
     * Mutator method for the 'score' variable.
     * 
     * @param  h    the new value of variable score
     */
    public void setScore(int s){
        score = s;
        checkWeapon();
    }
    
    /**
     * Mutator method for the 'numZombiesKilled' variable.
     * 
     * @param  h    the new value of variable numZombiesKilled
     */
    public void setZombieKilled(int z){
        numZombiesKilled = z;
    }
    
    /**
     * Method that checks if the player's score is high enough to upgrade the weapon
     */
    private void checkWeapon(){
        Player player = (Player) getWorld().getObjects(Player.class).get(0);
        if(score >= 100 && score < 200 && weaponLevelShotgun == false){
            player.setWeapon(1);
            weaponLevelShotgun = true;
            weaponUp();
        }
        if(score >= 200 && score < 300 && weaponLevelRifle == false){
            player.setWeapon(2);
            weaponLevelRifle = true;
            weaponUp();
        }
    }
    
    /**
     * Method that creates the "Weapon Upgrade" when the weapon leveled up.
     */
    private void weaponUp(){
        WeaponIncrease temp = new WeaponIncrease();
        getWorld().addObject(temp, 640, 460);
    }
    
    /**
     * Method that creates the black box that dims the screen.
     */
    private void dimScreen(){
        DimmerBox box = new DimmerBox();
        getWorld().addObject(box, 640, 360);
    }
}