import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.Random;

/**
 * The superclass for all Enemys. Contains the variables and methods shared by all Enemys.
 * 
 * @author Michael Zou, Chengze Cai
 * @version April 3, 2018
 */
public abstract class Enemy extends Actor
{
    //Variables used by the enemies.
    protected boolean touchingPlayer;
    protected int health;
    protected int speed;
    protected int damage;
    protected int attackCooldown;
    protected int score;
    
    //Variables that stores the images needed for the animation.
    protected GreenfootImage[] walk;
    protected GreenfootImage[] attack;
    
    //Variables that are used to control the animation.
    protected int animationCount;   //Counter for walking animation
    protected int walkAnimateSpeed; //Speed of walking animation
    protected int frame;            //Used to reference the walk frames
    protected int attackAnimationCount;   //Counter for attack animation
    protected int attackAnimateSpeed;     //Speed of the attack animation
    protected int attackFrame;            //Used to reference the attack frames
    
    /**
     * Constructor for objects of class Enemy, initializes the speed, damage and health of the enemy.
     */
    public Enemy(){
        touchingPlayer = false;
    }
    
    /**
     * Method that turns the enemy towards the player.
     */
    public void turnTowardsPlayer(){
        Player player = (Player) getWorld().getObjects(Player.class).get(0);  
        turnTowards(player.getX(), player.getY());  
    }
    
    /**
     * Method that checks if the enemy is touching the player, if it is, change the touching player to true.
     */
    public void checkTouching(){
        Player player = (Player) getWorld().getObjects(Player.class).get(0);  
        double distance = Math.sqrt(Math.pow(this.getX() - player.getX(), 2) + Math.pow(this.getY() - player.getY(), 2));  
        GameStats stats = (GameStats) getWorld().getObjects(GameStats.class).get(0);
        if(stats.getHealth() <= 0 && distance < 20)
            touchingPlayer = true;
        else if(distance < 100 && stats.getHealth() > 0)
            touchingPlayer = true;
        else 
            touchingPlayer = false;
    }    
    
    /**
     * Method that attacks the player after a fixed time period.
     */
    protected void attack(){
        if(attackCooldown == 0){
            GameWorld gameworld = (GameWorld) getWorld();
            GameStats stats = (GameStats) getWorld().getObjects(GameStats.class).get(0);
            stats.setHealth(stats.getHealth()-damage);
            attackCooldown = 60;
        }else{
            attackCooldown--;
        }
    }
    
    /**
     * Method that animates the walking animation of the zombie.
     */
    protected void animateWalk(){
        animationCount++;
        if(animationCount >= walkAnimateSpeed){
            if(frame >= walk.length)
                frame = 0;
            setImage(walk[frame]);
            frame++;
            animationCount = 0;
        }
    }
    
    /**
     * Method that animates the attacking animation of the zombie.
     */
    protected void animateAttack(){
        attackAnimationCount++;
        if(attackAnimationCount >= attackAnimateSpeed){
            if(attackFrame >= attack.length)
                attackFrame = 0;
            setImage(attack[attackFrame]);
            attackFrame++;
            attackAnimationCount = 0;
        }
    }
    
    /**
     * Method that reduce the health of the zombie.
     */
    public void takeDamage(int d){
        health -= d;
    }
    
    /**
     * Method that increases the overall score of the game.
     */
    public void increaseScore(){
        GameStats stats = (GameStats) getWorld().getObjects(GameStats.class).get(0);
        stats.setScore(stats.getScore() + score);
    }
    
    /**
     * Accessor method for the 'health' variable.
     * 
     * @return     the health variable
     */
    public int getHealth(){
        return health;
    }
}