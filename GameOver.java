import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Actor for Game Over text that displays when the player dies.
 * 
 * @author Chengze Cai, Brice Zhao
 * @version April 15, 2018
 */
public class GameOver extends Actor
{
    private boolean ended;
    private int transparency;
    GreenfootImage gameover;
    public GameOver()
    {
        transparency = 0;
        getImage().setTransparency(0);
        ended = false;
        
        gameover = new GreenfootImage("Game Over.png");
        setImage(gameover);
    }
    
    /**
     * Act - do whatever the GameOver wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        transparency += 2;
        if(transparency < 255)
        {
            getImage().setTransparency(transparency);
        }
        else if(!ended)
        {
            TryAgain tryagain = new TryAgain();
            getWorld().addObject(tryagain, 440, 450);
            
            MainMenu mainmenu = new MainMenu();
            getWorld().addObject(mainmenu, 840, 450);
            ended = true;
        }
    }  
}
