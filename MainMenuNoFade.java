import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.io.*;

/**
 * Button that takes the user to the Main Menu World from the instructions.
 * 
 * @author Chengze Cai, Brice Zhao, Michael Zou
 * @version April 15, 2018
 */
public class MainMenuNoFade extends Actor
{
    GreenfootImage menu;
    
    /**
     * Constructors for objects of class MainMenuNoFade, initializes the image.
     */
    public MainMenuNoFade(){
        menu = new GreenfootImage("Main Menu Button.png");
        setImage(menu);
    }
    
    /**
     * Act - do whatever the StartButton wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act(){
        try{
            if (Greenfoot.mouseClicked(this))
            {
                Greenfoot.setWorld(new StartScreen());
            }
        }
        catch(IOException ioe){
            Greenfoot.stop();
        }
    }    
}
