import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.ArrayList;
import java.util.*;

/**
 * Holds the actions and properties for the player entity.
 * 
 * @author Chengze Cai, Michael Zou
 * @version April 15, 2018
 */
public class Player extends Actor
{
    private boolean targetLocked = false;
    private int counter = 0;
    private boolean fired = true;
    private boolean isShooting = false;

    private int animationCount;   //Speed of the animation
    private int frame;            //Used to reference the walk frames

    private int shootAnimationCount;   //Speed of the animation
    private int shootFrame;            //Used to reference the shoot frames

    private int weapon = 0;
    private boolean newWeapon = false;

    private boolean ended = false;

    private GreenfootImage[][] idle;
    private GreenfootImage[][] shoot;

    GreenfootImage player;
    
    /**
     * Constructor for objects of class Player, initializes all the pictures used for animations.
     */
    public Player(){
        //initialize variables
        animationCount = 1;
        frame  = 1;
        player = new GreenfootImage("player1.png");
        setImage(player);

        idle = new GreenfootImage[3][20];
        for(int i = 0; i< 3; i++){
            if(i == 0){
                for(int j = 0; j< 20; j++){
                    idle[i][j] = new GreenfootImage("survivor-idle_handgun_" + i + ".png");
                }  
            }else if(i == 1){
                for(int j = 0; j< 20; j++){
                    idle[i][j] = new GreenfootImage("survivor-idle_shotgun_" + i + ".png");
                }  
            }else if(i == 2){
                for(int j = 0; j< 20; j++){
                    idle[i][j] = new GreenfootImage("survivor-idle_rifle_" + i + ".png");
                }  
            }
        }

        shoot = new GreenfootImage[3][3];
        for(int i = 0; i< 3; i++){
            if(i == 0){
                for(int j = 0; j < 3; j++){
                    shoot[i][j] = new GreenfootImage("survivor-shoot_handgun_" + i + ".png");
                } 
            }else if(i == 1){
                for(int j = 0; j < 3; j++){
                    shoot[i][j] = new GreenfootImage("survivor-shoot_shotgun_" + i + ".png");
                } 
            }else if(i== 2){
                for(int j = 0; j < 3; j++){
                    shoot[i][j] = new GreenfootImage("survivor-shoot_rifle_" + i + ".png");
                } 
            }
        }
    }

    /**
     * Act - do whatever the Player wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        if(!isShooting){
            animateIdle();
        }else{
            animateShoot();
        }

        turnTowardsEnemy();
        counter++;

        if(newWeapon){
            if(weapon == 1){
                newWeapon = false;
            }else if(weapon == 2){
                newWeapon = false;
            }
        }

        if(!fired){
            if(weapon == 0){
                getWorld().addObject(new PistolBullet(), getX(), getY());
                fired = true;
            }else if(weapon == 1){
                getWorld().addObject(new ShotgunBullet(), getX(), getY());
                getWorld().addObject(new ShotgunBullet(), getX(), getY());
                getWorld().addObject(new ShotgunBullet(), getX(), getY());
                fired = true;
            }else if(weapon == 2){
                getWorld().addObject(new RifleBullet(), getX(), getY());
                fired = true;
            }
        } 
        checkCollision();
    }

    /**
     * Method that turns the player to the closest enemy.
     */
    public void turnTowardsEnemy(){
        ArrayList<Enemy> enemies = (ArrayList<Enemy>) getWorld().getObjects(Enemy.class);
        if(!enemies.isEmpty()){
            Enemy closest = enemies.get(0);
            double distance = Math.sqrt(Math.pow(closest.getX() - this.getX(), 2) + Math.pow(closest.getY() - this.getY(), 2));
            for(Enemy ref : enemies){
                double temp = Math.sqrt(Math.pow(ref.getX() - this.getX(), 2) + Math.pow(ref.getY() - this.getY(), 2));
                if(temp < distance){
                    distance = temp;
                    closest = ref;
                }
            }
            turnTowards(closest.getX(), closest.getY());
        }
    }

    /**
     * Method that adjust shooting-related variables for other objects to manipulate.
     */
    public void shoot(){
        fired = false;
        isShooting = true;
    }

    /**
     * Method that animates the player when it's idle.
     */
    private void animateIdle(){
        animationCount++;
        if(animationCount >= 5){
            if(frame >= idle.length)
                frame = 0;
            setImage(idle[weapon][frame]);
            frame++;
            animationCount = 0;
        }
    }

    /**
     * Method that animates the player when it shoots.
     */
    private void animateShoot(){
        shootAnimationCount++;
        if(shootAnimationCount >= 5){
            if(shootFrame >= shoot.length){
                isShooting = false;
                shootFrame = 0;
            }
            setImage(shoot[weapon][shootFrame]);
            shootFrame++;
            shootAnimationCount = 0;
        }
    }

    /**
     * Mutator method for the 'weapon' variable.
     * 
     * @param  w    the new value of weapon 
     */
    public void setWeapon(int w){
        newWeapon = true;
        weapon = w;
    }

    /**
     * Accessor method for the 'weapon' variable.
     * 
     * @return     the weapon variable
     */
    public int getWeapon(){
        return weapon;
    }

    /**
     * Checks if the player is colliding with a zombie, if yes, end the game.
     */
    public void checkCollision(){
        Enemy a = (Enemy) getOneIntersectingObject(Enemy.class);
        if(a != null){ 
            WordManager manager= (WordManager) getWorld().getObjects(WordManager.class).get(0);
            manager.gameOver();
            if(!ended)
                dimScreen();
            ended = true;
            GreenfootImage blood = new GreenfootImage("PlayerBloodstain.png");
            setImage(blood);
        }
    }

    /**
     * Method that creates the black box that dims the screen.
     */
    private void dimScreen(){
        DimmerBox box = new DimmerBox();
        getWorld().addObject(box, 640, 360);
    }
}