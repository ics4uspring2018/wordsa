import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.ArrayList;
import java.util.*;
import java.io.*;

/**
 * Write a description of class StartScreen here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class StartScreen extends World
{

    /**
     * Constructor for objects of class StartScreen.
     * 
     */
    public StartScreen() throws IOException
    {    
        super(1280, 720, 1); 
        setBackground(new GreenfootImage("Start Screen.png"));
        prepare();
    }
    
    private void prepare() throws IOException
    {
        StartButton startButton = new StartButton();
        addObject(startButton,640,420);
        
        Instructions instruction = new Instructions();
        addObject(instruction, 640, 520);
    }
}
