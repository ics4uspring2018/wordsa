import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Warning indicator for when a big wave of enemies is about to come.
 * 
 * @author Chengze Cai, Michael Zou
 * @version April 15, 2018
 */
public class BigWaveWarning extends Actor
{
    int x;
    int y;
    int transparency;
    
    GreenfootImage wave;
    
    /**
     * Constructor for class BigWaveWarning
     **/
    public BigWaveWarning(){
        x = 0;
        y = 0;
        transparency = 255;
        
        wave = new GreenfootImage("bigwaveindicator.png");
        setImage(wave);
    }
    
    /**
     * Act - do whatever the weaponIncrease wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        x = getX();
        y = getY();
        setLocation(x,y-1);
        transparency = transparency - 5;
        getImage().setTransparency(transparency);
        if(transparency == 0){
            getWorld().removeObject(this);
            return;
        }
    }   
}
