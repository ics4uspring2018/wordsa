import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.io.*;

/**
 * Button to start the game.
 * 
 * @author Brice Zhao
 * @version April 15 2018
 */
public class StartButton extends Actor
{
    GreenfootImage startbutton;
    /**
     * Constructor for the class StartButton.
     **/
    public StartButton(){
        startbutton = new GreenfootImage("Start Button.png");
        setImage(startbutton);
    }
    
    /**
     * Act - do whatever the StartButton wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act(){
        try{
            if (Greenfoot.mouseClicked(this))
            {
                Greenfoot.setWorld(new GameWorld());
            }
        }
        catch(IOException ioe){
            Greenfoot.stop();
        }
    }    
}
