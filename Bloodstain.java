import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.Random;

/**
 * An object that is created when a zombie is destroyed and fizzles out after a while.
 * 
 * @author Michael Zou
 * @version April 15, 2018
 */
public class Bloodstain extends Actor
{
    int transparency;
    GreenfootImage stain;
    
    /**
     * Constructors of objects Bloodstain, initializes the image and the variables.
     */
    public Bloodstain(){
        //set image to be the bloodstain
        stain = new GreenfootImage("ZombieBloodstain.png");
        setImage(stain);
        
        //set the rotation to be a random number from 0 to 359
        Random rand = new Random();
        setRotation(rand.nextInt(360));
        
        transparency = 240;        
    }
    
    /**
     * Act - do whatever the BloodStain wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        transparency -= 5;
        getImage().setTransparency(transparency);
        if(transparency == 0){
            getWorld().removeObject(this);
            return;
        }
    }    
}
